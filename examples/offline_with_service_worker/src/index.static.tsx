import React from 'react';
import ReactDom from 'react-dom'

function App() {
    return (
        <>
            <h1>Render Page</h1>
            <p>This text is a basic react app rendering after the page loads</p>
            <p>It is constructed from a basic pug file (index.static.pug => index.html) loading a basic TypeScript file (index.static.tsx => index.js)</p>
            <hr />
            <p>This page uses service workers. This is something currently being played with.</p>
            <p>One thought is to automatically generate a manifest file which contains a map of static entries.</p>
            <p>Ideally such a manifest would also list all other files, so that they can be invalidated or preloaded.</p>
            <p>Call it manifest.json</p>
        </>
    )
}

window.addEventListener("DOMContentLoaded", () => {
    ReactDom.render(<App />, document.querySelector(".root"))
})