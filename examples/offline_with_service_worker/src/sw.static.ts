/// <reference types="../../node_modules/types-serviceworker" />
console.log("SERVICE WORKER")

import preval from 'preval.macro';

console.log("Preval result:", preval(`
const hashFileTree = require('hash-file-tree').default;
const results = hashFileTree(__dirname).then(console.log);
module.exports = results;
`).then(console.log));

const cacheName = 'cache-v1.0.98';

const cacheContent = [
    './index.js',
    './index.html',
    './',
    './favicon.ico'
];

const cache_names = [ cacheName ];

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(cacheName)
      .then(cache => cache.addAll(cacheContent))
      // .then(self.skipWaiting()) // Some google examples do this. Unclear why. Effectively promise.all?
  );
});

// The activate handler takes care of cleaning up old caches.
self.addEventListener('activate', event => {
  event.waitUntil(
    caches.keys()
      .then(existing_cache_names => {
        return Promise.all(
            existing_cache_names
            .filter(cache_name => !cache_names.includes(cache_name))
            .map(cache_name => caches.delete(cache_name))
        );
      })
      // @ts-ignore
      .then(() => self.clients.claim())
  );
});

self.addEventListener('message', function (event) {
  if (event.data.action === 'skipWaiting') {
    // @ts-ignore
    self.skipWaiting();
  }
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        if (response) {
          return response;
        } else {
          return fetch(event.request);
        }
      })
  );
});