import React from 'react';
import ReactDom from 'react-dom'

function App() {
    return (
        <>
            <h1>Render Page</h1>
            <p>This text is a basic react app rendering after the page loads</p>
            <p>It is constructed from a basic pug file (index.static.pug => index.html) loading a basic TypeScript file (index.static.tsx => index.js)</p>
        </>
    )
}

window.addEventListener("DOMContentLoaded", () => {
    ReactDom.render(<App />, document.querySelector(".root"))
})