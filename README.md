# Web App Examples

Examples for myself for how to do web app stuff. Initially centering on React and norm-loader

# Static Page

At [static_page](./examples/static_page) a plain pug file is rendered into a single static page.

# Basic Render

At [render_page](./examples/render_page) a basic React app is rendered into the page

## TODO

- Render app to page
- static pages
- manifests
- shared chunks
- delayed loading
- server side rendering per request
- pre-rendering for all requests