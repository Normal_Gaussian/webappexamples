#!/usr/bin/env node
const path = require('path');
const express = require('express');
let port = Number(process.argv[1]) || 3000;

const finalUrl = `http://localhost:${port}/`;

const app = express();

const fs = require('fs');
const exampleDirectories = fs.readdirSync(path.join(__dirname, 'examples'));
const staticDirectories = exampleDirectories.map(example => [example, path.join('examples', example, 'dist')])

staticDirectories.forEach(([example, dir]) => app.use(`/${example}`, express.static(dir)));

app.route('/').get((req, res, next) => {
  res.send(`
    <ul>
      ${staticDirectories.map(([example, dir]) => `<li><a href="/${example}">${example}</a></li>`).join('\n')}
    </ul>
  `)
})

function startListening() {
  try {
    app.listen(
      port,
      () => {
        console.log(`Serving:\n${staticDirectories.map(x => `\t${x[0]} @ ${x[1]}`).join('\n')}\nat ${finalUrl}`)
      }
    )
  } catch(e) {
    console.error("This does not fire despite this function being in the stack trace")
  }
}

app.on('error', (e) => {
  if (e.code === 'EADDRINUSE') {
    console.error(`Failed to serve ${staticDirectories} at ${finalUrl}\n`, e)
    console.log(`Address ${finalUrl} in use, retrying...`);
    setTimeout(() => {
        app.close()
        startListening()
    }, 1000);
  }
});

startListening();